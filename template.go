package main

import (
	"html/template"
	"net/http"
	"regexp"
)

var templates = template.Must(template.ParseFiles("tmpl/edit.html", "tmpl/view.html"))

func renderTemplate(w http.ResponseWriter, tmpl string, p *Page) {
	err := templates.ExecuteTemplate(w, tmpl+".html", p)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func renderTemplateWithHTML(w http.ResponseWriter, tmpl string, p *Page) {
	t := templates.Lookup(tmpl + ".html")
	ps := struct {
		Title string
		Body  template.HTML
	}{}
	ps.Title = p.Title
	br := insertPageRefs(p.Body)
	ps.Body = template.HTML(br)
	err := t.Execute(w, ps)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func insertPageRefs(body []byte) []byte {
	search := regexp.MustCompile("\\[[a-zA-Z]+\\]")
	body = search.ReplaceAllFunc(body, func(b []byte) []byte {
		pr := string(b[1 : len(b)-1])
		return []byte("<a href=\"/view/" + pr + "\">" + pr + "</a>")
	})
	return body
}
