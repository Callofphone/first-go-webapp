package main

import (
	"log"
	"net/http"
)

func main() {
	addHandlers()
	log.Fatal(http.ListenAndServe(":8080", nil))
}
